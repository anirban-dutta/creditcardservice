package com.sjt.creditcardservice.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sjt.creditcardservice.exception.InsufficientBalanceException;
import com.sjt.creditcardservice.model.AllUserReport;
import com.sjt.creditcardservice.model.CreditAppResponse;
import com.sjt.creditcardservice.model.MonthlyTransactions;
import com.sjt.creditcardservice.model.TransactionInput;
import com.sjt.creditcardservice.model.UserTransactions;
import com.sjt.creditcardservice.service.impl.CreditAppServiceImpl;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(value="/creditapp")
@Api(value="Credit Service App", description="This service is part of Sujosu's coding round")
public class CreditAppController {
	
	@Autowired
	private CreditAppServiceImpl creditAppServiceImpl;
	
	@PostMapping(value="/transact/{userId}",produces="application/json")
	@ApiOperation(value = "Perform either a debit or credit transaction", response = CreditAppResponse.class)
	public CreditAppResponse performTransaction(@PathVariable long userId, @ModelAttribute TransactionInput transactionInput) throws InsufficientBalanceException {
		return creditAppServiceImpl.performTransaction(userId,transactionInput);
	}
	
	@GetMapping(value="report/users",produces="application/json")
	@ApiOperation(value = "Gives us a report for all users and their total credits and debits", response = List.class)
	public List<AllUserReport> allUserReport() {
		return creditAppServiceImpl.getAllUserReport();
	}
	
	@GetMapping(value="report/{userId}",produces="application/json")
	@ApiOperation(value = "Gives us a report for a particular user", response = UserTransactions.class)
	public UserTransactions userReport(@PathVariable Long userId) {
		return creditAppServiceImpl.getUserReport(userId);
	}
	
	@GetMapping(value="report/monthly",produces="application/json")
	@ApiOperation(value = "Gives us a monthly report for all users", response = List.class)
	public List<MonthlyTransactions> monthlyReport() {
		return creditAppServiceImpl.getMonthlyReport();
	}

}
