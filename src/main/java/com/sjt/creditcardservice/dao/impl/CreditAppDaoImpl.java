package com.sjt.creditcardservice.dao.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.sjt.creditcardservice.dao.CreditAppDao;
import com.sjt.creditcardservice.dao.rowmapper.AllUserReportRowMapper;
import com.sjt.creditcardservice.dao.rowmapper.MonthlyReportRowMapper;
import com.sjt.creditcardservice.dao.rowmapper.UserTransactionReportRowMapper;
import com.sjt.creditcardservice.model.AllUserReport;
import com.sjt.creditcardservice.model.MonthlyTransactions;
import com.sjt.creditcardservice.model.TransactionInput;
import com.sjt.creditcardservice.model.Transactions;

@Repository
public class CreditAppDaoImpl implements CreditAppDao {
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	private static final String INSERT_TRANSACTION = "INSERT INTO transactions (user_id, transaction_type, transaction_amount, transaction_currency) VALUES (?,?,?,?)";
	private static final String INCREASE_BALANCE = "UPDATE account_master set account_balance = account_balance + ? where user_id = ?";
	private static final String DEDUCT_BALANCE = "UPDATE account_master set account_balance = account_balance - ? where user_id = ?";	
	private static final String GET_BALANCE = "SELECT account_balance from account_master where user_id = ?";	
	private static final String ALL_USER_REPORT = "select am.user_id, total_debits, total_credits, transaction_currency, account_balance\r\n" + 
			"from (SELECT \r\n" + 
			"acm.user_id,\r\n" + 
			"SUM(case when transaction_type='D' then transaction_amount else 0 end) AS total_debits, \r\n" + 
			"SUM(case when transaction_type='C' then transaction_amount else 0 end) AS total_credits,\r\n" + 
			"transaction_currency, account_balance\r\n" + 
			"FROM transactions t \r\n" + 
			"join account_master acm\r\n" + 
			"GROUP BY acm.user_id) am\r\n";	
	
	private static final String USER_TRANSACTION_REPORT = "SELECT am.card_number,t.transaction_currency,t.transaction_amount,\r\n" + 
			"case when t.transaction_type='C' then 'CREDIT' else 'DEBIT' end as transaction_type,\r\n" + 
			"t.transaction_time\r\n" + 
			"from transactions t\r\n" + 
			"join account_master am on t.user_id = am.user_id where t.user_id = ?";
	
	private static final String MONTHLY_REPORT = "select month,transaction_currency, total_debits, total_credits\r\n" + 
			"from (SELECT \r\n" + 
			"concat(monthname(transaction_time),year(transaction_time)) as month,transaction_currency,\r\n" + 
			"SUM(case when transaction_type='D' then transaction_amount else 0 end) AS total_debits, \r\n" + 
			"SUM(case when transaction_type='C' then transaction_amount else 0 end) AS total_credits \r\n" + 
			"FROM transactions \r\n" + 
			"GROUP BY concat(monthname(transaction_time),year(transaction_time))) x";
	
	@Override
	public Long performTransaction(long userId, TransactionInput transactionInput) {
		
		int rowsAffected = jdbcTemplate.update(INSERT_TRANSACTION, new Object[] {userId, transactionInput.getTransactionType(),transactionInput.getTransactionAmount(),
				transactionInput.getTransactionCurrency()});
		
		return rowsAffected == 1 ? userId : null;
	}

	public void updateAccountBalance(Long userID, String transactionType, float transactionAmount) {
		
		if (transactionType.equals("C")) {
			jdbcTemplate.update(INCREASE_BALANCE, new Object[] {transactionAmount,userID});
		} else {
			jdbcTemplate.update(DEDUCT_BALANCE, new Object[] {transactionAmount,userID});
		}
	}

	public Float getAccountBalance(Long userID) {
		return jdbcTemplate.queryForObject(GET_BALANCE, new Object[] {userID}, Float.class);
	}

	public List<AllUserReport> getAllUserReport() {
		return jdbcTemplate.query(ALL_USER_REPORT, new AllUserReportRowMapper());
	}

	public List<Transactions> getUserTransactions(Long userId) {
		return jdbcTemplate.query(USER_TRANSACTION_REPORT, new Object[] {userId}, new UserTransactionReportRowMapper());
	}

	public List<MonthlyTransactions> getMonthlyReport() {
		
		return jdbcTemplate.query(MONTHLY_REPORT, new MonthlyReportRowMapper());
	}

}
