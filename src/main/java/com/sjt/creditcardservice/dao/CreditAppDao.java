package com.sjt.creditcardservice.dao;

import java.util.List;

import com.sjt.creditcardservice.model.AllUserReport;
import com.sjt.creditcardservice.model.MonthlyTransactions;
import com.sjt.creditcardservice.model.TransactionInput;
import com.sjt.creditcardservice.model.Transactions;

public interface CreditAppDao {
	public Long performTransaction(long userId, TransactionInput transactionInput);
	
	public void updateAccountBalance(Long userID, String transactionType,float transactionAmount);
	
	public Float getAccountBalance(Long userId);
	
	public List<AllUserReport> getAllUserReport();
	
	public List<Transactions> getUserTransactions(Long userId);
	
	public List<MonthlyTransactions> getMonthlyReport();
}
