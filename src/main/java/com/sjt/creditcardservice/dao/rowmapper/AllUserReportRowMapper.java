package com.sjt.creditcardservice.dao.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.sjt.creditcardservice.model.AllUserReport;

public class AllUserReportRowMapper implements RowMapper<AllUserReport> {

	@Override
	public AllUserReport mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		AllUserReport allUserReport = new AllUserReport();
		allUserReport.setUserId(rs.getLong("user_id"));
		allUserReport.setCurrency(rs.getString("transaction_currency"));
		allUserReport.setTotalCredits(rs.getFloat("total_credits"));
		allUserReport.setTotalDebits(rs.getFloat("total_debits"));
		allUserReport.setBalance(rs.getFloat("account_balance"));
		return allUserReport;
	}

}
