package com.sjt.creditcardservice.dao.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.sjt.creditcardservice.model.Transactions;

public class UserTransactionReportRowMapper implements RowMapper<Transactions> {

	@Override
	public Transactions mapRow(ResultSet rs, int rowNum) throws SQLException {
		Transactions transactions = new Transactions();
		
		transactions.setCardNumber(rs.getString("card_number"));
		transactions.setCurrency(rs.getString("transaction_currency"));
		transactions.setAmount(rs.getFloat("transaction_amount"));
		transactions.setType(rs.getString("transaction_type"));
		transactions.setTimestamp(rs.getString("transaction_time"));
		
		return transactions;
	}

}
