package com.sjt.creditcardservice.dao.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.sjt.creditcardservice.model.MonthlyTransactions;

public class MonthlyReportRowMapper implements RowMapper<MonthlyTransactions>{

	@Override
	public MonthlyTransactions mapRow(ResultSet rs, int rowNum) throws SQLException {
		MonthlyTransactions monthlyTransactions = new MonthlyTransactions();
		monthlyTransactions.setMonth(rs.getString("month"));
		monthlyTransactions.setCurrency(rs.getString("transaction_currency"));
		monthlyTransactions.setTotalCredits(rs.getFloat("total_credits"));
		monthlyTransactions.setTotalDebits(rs.getFloat("total_debits"));
		
		return monthlyTransactions;
	}

}
