package com.sjt.creditcardservice.exception;

import lombok.Data;

@Data
public class InsufficientBalanceException extends Exception {

	private static final long serialVersionUID = 7350425368089683822L;
	
	private String message;
	private Float accountBalance;
	private Float transactionAmount;

	public InsufficientBalanceException(String message, Float accountBalance, Float transactionAmount) {
		super();
		this.message = message;
		this.accountBalance = accountBalance;
		this.transactionAmount = transactionAmount;
	}
	
}
