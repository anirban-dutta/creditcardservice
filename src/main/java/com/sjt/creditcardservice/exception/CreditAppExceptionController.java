package com.sjt.creditcardservice.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.sjt.creditcardservice.model.InsufficientBalance;

@ControllerAdvice
public class CreditAppExceptionController {
	
	@ExceptionHandler(value = InsufficientBalanceException.class)
	public ResponseEntity<InsufficientBalance> handleException(InsufficientBalanceException insufficientBalanceException) {
		InsufficientBalance insufficientBalance = new InsufficientBalance();
		insufficientBalance.setMessage("Insufficient funds in your account !!");
		insufficientBalance.setAccountBalance(insufficientBalanceException.getAccountBalance());
		insufficientBalance.setEnteredAmount(insufficientBalanceException.getTransactionAmount());
		return new ResponseEntity<>(insufficientBalance,HttpStatus.BAD_REQUEST);
	}
}
