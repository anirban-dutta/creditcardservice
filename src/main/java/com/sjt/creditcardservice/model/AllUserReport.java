package com.sjt.creditcardservice.model;

import java.io.Serializable;

import lombok.Data;

@Data
public class AllUserReport implements Serializable {
	
	private static final long serialVersionUID = -9184938551959773903L;
	private Long userId;
	private String currency;
	private Float balance;
	private Float totalCredits;
	private Float totalDebits;
}
