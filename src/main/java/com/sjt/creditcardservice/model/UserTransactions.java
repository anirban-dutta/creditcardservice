package com.sjt.creditcardservice.model;

import java.io.Serializable;
import java.util.List;

import lombok.Data;

@Data
public class UserTransactions implements Serializable {
	private static final long serialVersionUID = -3751993468074348356L;
	private Long userId;
	private List<Transactions> transactions;
}
