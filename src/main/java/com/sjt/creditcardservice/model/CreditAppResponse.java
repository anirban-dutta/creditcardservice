package com.sjt.creditcardservice.model;

import java.io.Serializable;

import lombok.Data;

@Data
public class CreditAppResponse implements Serializable {

	private static final long serialVersionUID = -814745796513693536L;
	private String status;
	private String message;
	private Float accountBalance;
}
