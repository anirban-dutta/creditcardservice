package com.sjt.creditcardservice.model;

import lombok.Data;

@Data
public class InsufficientBalance {
 private String message;
 private Float accountBalance;
 private Float enteredAmount;
}
