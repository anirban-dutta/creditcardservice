package com.sjt.creditcardservice.model;

import java.io.Serializable;

import lombok.Data;

@Data
public class Transactions implements Serializable {
	private static final long serialVersionUID = -2405654078071447828L;
	private String cardNumber;
	private String timestamp;
	private String type;
	private String currency;
	private float amount;
}
