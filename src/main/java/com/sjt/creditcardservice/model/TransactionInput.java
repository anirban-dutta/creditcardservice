package com.sjt.creditcardservice.model;

import java.io.Serializable;

import lombok.Data;

@Data
public class TransactionInput implements Serializable{

	private static final long serialVersionUID = -4910551025810318200L;
	private String transactionType;
	private float transactionAmount;
	private String transactionCurrency;

}
