package com.sjt.creditcardservice.model;

import java.io.Serializable;

import lombok.Data;

@Data
public class MonthlyTransactions implements Serializable {
	private static final long serialVersionUID = -2279612349971045032L;
	private String month;
	private String currency;
	private Float totalCredits;
	private Float totalDebits;
}
