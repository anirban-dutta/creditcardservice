package com.sjt.creditcardservice.service;

import java.util.List;

import com.sjt.creditcardservice.exception.InsufficientBalanceException;
import com.sjt.creditcardservice.model.AllUserReport;
import com.sjt.creditcardservice.model.CreditAppResponse;
import com.sjt.creditcardservice.model.MonthlyTransactions;
import com.sjt.creditcardservice.model.TransactionInput;
import com.sjt.creditcardservice.model.UserTransactions;

public interface CreditAppService {
	
	public CreditAppResponse performTransaction(long userId,TransactionInput transactionInput) throws InsufficientBalanceException;
	
	public List<AllUserReport> getAllUserReport();
	
	public UserTransactions getUserReport(Long userId);
	
	public List<MonthlyTransactions> getMonthlyReport();

}
