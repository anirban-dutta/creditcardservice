package com.sjt.creditcardservice.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sjt.creditcardservice.dao.impl.CreditAppDaoImpl;
import com.sjt.creditcardservice.exception.InsufficientBalanceException;
import com.sjt.creditcardservice.model.AllUserReport;
import com.sjt.creditcardservice.model.CreditAppResponse;
import com.sjt.creditcardservice.model.MonthlyTransactions;
import com.sjt.creditcardservice.model.TransactionInput;
import com.sjt.creditcardservice.model.UserTransactions;
import com.sjt.creditcardservice.service.CreditAppService;

@Service
public class CreditAppServiceImpl implements CreditAppService {
	
	@Autowired
	private CreditAppDaoImpl creditAppDaoImpl;
	

	@Override
	public CreditAppResponse performTransaction(long userId, TransactionInput transactionInput) throws InsufficientBalanceException {

		Long userID = creditAppDaoImpl.performTransaction(userId,transactionInput);
		
		if(transactionInput.getTransactionType().equals("D")) {
			Float accountBalance = creditAppDaoImpl.getAccountBalance(userID);
			if(transactionInput.getTransactionAmount() > accountBalance) {
				throw new InsufficientBalanceException("Insufficient balance",accountBalance,transactionInput.getTransactionAmount());
			}
		}
			
		creditAppDaoImpl.updateAccountBalance(userID,transactionInput.getTransactionType(),transactionInput.getTransactionAmount());
			
		return prepareAppResponse(creditAppDaoImpl.getAccountBalance(userID));
	}


	private CreditAppResponse prepareAppResponse(Float balance) {
		CreditAppResponse creditAppResponse = new CreditAppResponse();
			creditAppResponse.setStatus("Success");
			creditAppResponse.setMessage("Transaction completed successfully!!");
			creditAppResponse.setAccountBalance(balance);
		return creditAppResponse;
	}


	public List<AllUserReport> getAllUserReport() {
		return creditAppDaoImpl.getAllUserReport();
	}


	public UserTransactions getUserReport(Long userId) {
		UserTransactions userTransactions = new UserTransactions();
		userTransactions.setUserId(userId);
		userTransactions.setTransactions(creditAppDaoImpl.getUserTransactions(userId));
		
		return userTransactions;
	}


	public List<MonthlyTransactions> getMonthlyReport() {
		return creditAppDaoImpl.getMonthlyReport();
	}

}
