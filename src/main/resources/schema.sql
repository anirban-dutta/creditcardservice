drop table if exists account_master;
drop table if exists transactions;

CREATE TABLE IF NOT EXISTS account_master(
user_id bigint,
card_number varchar(19) primary key,
account_balance decimal(13,2)
);

CREATE TABLE IF NOT EXISTS transactions(
  transaction_id bigint auto_increment primary key,
  user_id bigint,
  transaction_type char(1),
  transaction_amount decimal(13, 2),
  transaction_currency char(3),
  transaction_time timestamp default current_timestamp
);